# Mamiza's Wallpaper collection.

This is my wallpaper collocation. If any of these images are yours and of limited use, please let
me know and I will remove it.

# Installation

Since the repository is huge (and gets bigger as I add more images) It's really important that
you specify the `--depth 1` option when cloning the repository:

``` shell
git clone --depth 1 https://gitlab.com/themamiza/wallpapers
```
